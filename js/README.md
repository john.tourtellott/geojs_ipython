Visualization of Geospatial Data

Package Install
---------------

**Prerequisites**
- [node](http://nodejs.org/)

```bash
npm install --save geojs_ipython
```
