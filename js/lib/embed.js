// Entry point for the unpkg bundle containing custom model definitions.
//
// It differs from the notebook bundle in that it does not need to define a
// dynamic baseURL for the static assets and may load some css that would
// already be loaded by the notebook otherwise.

// Export widget models and views, and the npm package version number.
var example = require('./example.js');
var geojs_widget = require('./geojs_widget.js');

module.exports = {
    GeoJSModel: geojs_widget.GeoJSModel,
    GeoJSView: geojs_widget.GeoJSView,
    HelloModel : example.HelloModel,
    HelloView : example.HelloView
};

module.exports['version'] = require('../package.json').version;
