// Export widget models and views, and the npm package version number.
var example = require('./example.js');
var geojs_widget = require('./geojs_widget.js');

module.exports = {
    GeoJSModel: geojs_widget.GeoJSModel,
    GeoJSView: geojs_widget.GeoJSView,
    HelloModel : example.HelloModel,
    HelloView : example.HelloView
};
module.exports['version'] = require('../package.json').version;
