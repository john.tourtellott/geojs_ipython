var widgets = require('@jupyter-widgets/base');
var _ = require('lodash');
var geo = require('geojs');


// When serialiazing the entire widget state for embedding, only values that
// differ from the defaults will be specified.
var GeoJSModel = widgets.DOMWidgetModel.extend({
    defaults: _.extend(widgets.DOMWidgetModel.prototype.defaults(), {
        _model_name : 'GeoJSModel',
        _view_name : 'GeoJSView',
        _model_module : 'geojs_ipython',
        _view_module : 'geojs_ipython',
        _model_module_version : '0.1.0',
        _view_module_version : '0.1.0'
    })
});


// Custom View. Renders the widget model.
var GeoJSView = widgets.DOMWidgetView.extend({
    initialize: function(parameters) {
        console.log('initialize GeoJSView');
        GeoJSView.__super__.initialize.apply(this, arguments);
        this.map = null;
        if (this.el) {
            this.map = geo.map({'node': this.el});
            this.map.createLayer('osm');
        }
    },

    render: function() {
        console.log('render GeoJSView');
        if (this.map) {
            this.map.draw();
        }
    }
});


module.exports = {
    GeoJSModel : GeoJSModel,
    GeoJSView : GeoJSView
};
