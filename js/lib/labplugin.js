var geojs_ipython = require('./index');
var base = require('@jupyter-widgets/base');

module.exports = {
  id: 'geojs_ipython',
  requires: [base.IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'geojs_ipython',
          version: geojs_ipython.version,
          exports: geojs_ipython
      });
  },
  autoStart: true
};

