print('loading geojs_ipython')
from ._version import version_info, __version__

from .example import *
from .geojs_widget import *

def _jupyter_nbextension_paths():
    return [{
        'section': 'notebook',
        'src': 'static',
        'dest': 'geojs_ipython',
        'require': 'geojs_ipython/extension'
    }]
