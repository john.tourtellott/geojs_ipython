import ipywidgets as widgets
from traitlets import Unicode
from traitlets import default
from traitlets import List


class GeoJSWidget(widgets.DOMWidget):
    _view_name = Unicode('GeoJSView').tag(sync=True)
    _model_name = Unicode('GeoJSModel').tag(sync=True)
    _view_module = Unicode('geojs_ipython').tag(sync=True)
    _model_module = Unicode('geojs_ipython').tag(sync=True)

    @default('layout')
    def _default_layout(self):
        return widgets.Layout(height='400px', align_self='stretch')
