geojs_ipython
===============================

Visualization of Geospatial Data

Installation
------------

To install use pip:

    $ pip install geojs_ipython
    $ jupyter nbextension enable --py --sys-prefix geojs_ipython


For a development installation (requires npm),

    $ git clone https://github.com//geojs_ipython.git
    $ cd geojs_ipython
    $ pip install -e .
    $ jupyter nbextension install --py --symlink --sys-prefix geojs_ipython
    $ jupyter nbextension enable --py --sys-prefix geojs_ipython
